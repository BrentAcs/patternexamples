﻿using NUnit.Framework;
using PatternExamples.Creational;

namespace PatternExamples.Behavioral
{
   [TestFixture]
   public class StrategyDemo
   {
      [Test]
      public void Demo()
      {
      }
   }

   public interface IPdfObjectStrategy
   {
      bool CanHandle();
      void Handle();
   }

   public class PdfObjectStrategy : IPdfObjectStrategy
   {
      public bool CanHandle()
      {
         throw new System.NotImplementedException();
      }

      public void Handle()
      {
         throw new System.NotImplementedException();
      }
   }
}