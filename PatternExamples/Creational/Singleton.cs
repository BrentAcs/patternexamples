﻿using System.Diagnostics;
using NUnit.Framework;

namespace PatternExamples.Creational
{
   [TestFixture]
   public class SingletonDemo
   {
      [Test]
      public void Demo()
      {
         // note, object instantiation via 'new' is not allowed, compiler error if uncommented.
         //SampleSingleton singleton = new SampleSingleton();

         // invoke Foo# off static Instance property
         SampleSingleton.Instance.Foo();
      }
   }

   public sealed class SampleSingleton
   {
      // Single, private reference for singleton
      private static readonly SampleSingleton _instance = new SampleSingleton();

      // Private c-tor to prevent object creation outside our implementation
      private SampleSingleton()
      {
      }

      // Public static 'Instance' property to guarantee creation of single instance of this object. 
      // Note, some implementations use a GetInstance() method instead of property.
      public static SampleSingleton Instance
      {
         get { return _instance; }
      }

      // Typical, example Foo() method
      public void Foo()
      {
         Debug.WriteLine( @"Foo was called" );
      }
   }
}
